<?php
	if (isset($_GET['i'])) {
    	if ($_GET['i'] == 4) {
        	$i = "pastel.css";
        } else if ($_GET['i'] == 3) {
        	$i = "coffee.css";
        } else if ($_GET['i'] == 2) {
        	$i = "green.css";
        } else {
        	$i = "red.css";
        }
    } else {
    	$i = "red.css";
    }
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <!-- Website Title & Description for Search Engine purposes -->
    <title>Home | Simpleton</title>
    <meta name="description" content="Simpleton Web Theme by Jesse Hellman">

    <!-- Mobile viewport optimized -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-glyphicons.css" rel="stylesheet">

    <!-- Author CSS -->
    <link href="css/<?= $i; ?>" rel="stylesheet">
    <style>
		#backto {
			font-size: 12px;
			display: block;
			margin-bottom: 7px;
			color: #fff;
			text-shadow: 1px 1px 1px #000;
		}
	</style>

    <!-- HTML5 Element Support For Older IE -->
    <script src="js/modernizr-2.6.2.min.js"></script>

</head>


<body>
<main id="allcontainer">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an<strong>old</strong> browser. Please <a href="http://browsehappy.com/">update your browser</a> to get the most of this website</p>
<![endif]-->

<!-- HEADER -->
<header id="topbar" class="row">
    <div id="logo" class="col-sm-6">
        <img src="images/office.png" height="81" alt="Logo" />
        <div id="logo_text">MMy Logo</div>
        <div id="logo_subtitle">Slogan</div>

    </div><!-- end #logo -->
    <nav class="col-sm-6" role="navigation">
        <ul>
            <li><a href="index.php">Posts</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
    </nav>
</header><!-- end #topbar -->

<!-- MAIN CONTENT -->

<div id="maincontent" class="row container">

    <!-- ARTICLE -->
    <article class="col-sm-8">
        <div id="home" class="row">
            <h1>Home | My Blog</h1>

            <section>
                <img class="img-responsive img-thumbnail" src="images/red-kite.jpg"/>
                <h4>Welcome</h4>
                <p>Lorem ipsum dolor sit amet, cu dicant eripuit civibus ius, his id suas ludus oportere, his commodo perpetua recteque ut. Nam in habeo summo mediocritatem, usu ut latine denique. Ad usu simul tantas indoctum, ignota commune interpretaris ei vis. No quot omnesque vel, ne quo quot dolor putent.</p>

				<p>Veri falli deseruisse nec ei, mel adhuc intellegat at. Sanctus appareat quo ne, id cum tation pertinax, vis omnium dolorum ne. Incorrupte omittantur philosophia id mel, at harum lucilius usu. Legere habemus imperdiet te mei, eum percipit democritum assueverit ne. An duo albucius tincidunt. Ad melius iuvaret sit. Eu nisl instructior nam, cum no alia debet consetetur.</p>
            </section>
        </div><!-- end #home -->

        <div id="register" class="row">
            <h2>Subscribe</h2>
            <section>
                <h5>Subscribe to our newsletter!</h5>
                <div id="register_box">
                        <form>
                            <input type="email" placeholder="Email" /><br />
                            <input type="submit" value="Subscribe!" />
                        </form>
                </div>
            </section>

        </div><!-- end #register -->
    </article>

    <!-- SIDEBAR -->
    <aside id="sidebar" class="col-sm-4">
        <div id="recent" class="row">
            <h3>Recents Posts</h3>
            <section>
                <ul>
                    <li><a href="#">Post 1</a></li>
                    <li><a href="#">Post 2</a></li>
                </ul>
            </section>

        </div><!-- end #recent -->
        <div id="popular" class="row">
            <h3>Popular Posts</h3>
            <section>
                <ul>
                	<li><a href="#">Post 1</a></li>
                    <li><a href="#">Post 2</a></li>
                    <li><a href="#">Post 3</a></li>
                </ul>
            </section>
        </div><!-- end #popular -->
        <div id="categories" class="row">
            <h3>Themes</h3>
            <section>
                <form>
                    <input type="text" placeholder="Search blog..." /><button><span class="glyphicon glyphicon-search"></span></button>
                </form>
                <ul>
                    <li><a href="index.php?i=1">Modern red</a></li>
                    <li><a href="index.php?i=2">Green</a></li>
                    <li><a href="index.php?i=3">Coffee</a></li>
                    <li><a href="index.php?i=4">Pastel</a></li>
                </ul>

            </section>
        </div><!-- end #categories -->
    </aside><!-- end #sidebar -->

</div><!-- end #maincontent -->

<!-- FOOTER -->
<footer class="row">
    <p>Copyright&copy; 2015 Jesse Hellman | All rights reserved </p>
</footer>

</main>

<!-- JQUERY AND BOOTSTRAP SCRIPTS -->

<script src="http://code.jquery.com/jquery.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.8.2.min.js"><\/script>')</script>

<!-- Bootstrap JS -->
<script src="js/bootstrap.min.js"></script>

<!-- Author JS -->
<script src="js/script.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

</body>
</html>
