	$(document).ready(function(){
        //start plugin
        $(".stars").rating({
            php	           : '../admin/php/manager.php', 	//path to manager.php file relative to HTML document. Not required in Display-only and Database-free modes.
            skin	       : '../skins/skin.png', 		//path to skin file relative to HTML document
            animate        : true,       //apply animation
            textmain       : '%ms (%v votes)',      //main text, shown then stats are loaded
            texthover      : 'very bad|bad|average|good|excellent'
        });
    });