/*

Custom JS
============

Author:  Jesse Hellman
Created: 28.09.2014

*/

$(function() {

    $('#alertMe').click(function(e) {
        e.preventDefault();

        $('#successAlert').slideDown();
    });

    $('a.pop').click(function(e) {
        e.preventDefault();
    });

    $('a.pop').popover();

    $('[rel="tooltip"]').tooltip();

});

/*
* RefTagger
*/

var refTagger = {
    settings: {
        bibleVersion: "NKJV",
        roundCorners: true,
        tooltipStyle: "dark",
        customStyle : {
            heading: {
                backgroundColor : "#267A99",
                color : "#00060D"
            },
            body   : {
                color : "#EEEEEE",
                moreLink : {
                    color: "#012340"
                }
            }
        }
    }
};
(function(d, t) {
    var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
    g.src = "//api.reftagger.com/v2/RefTagger.js";
    s.parentNode.insertBefore(g, s);
}(document, "script"));

/*
* Stars
*/
$(document).ready(function(){
        //start plugin
        $(".stars").rating({
            php	           : '../admin/php/manager.php', 	//path to manager.php file relative to HTML document. Not required in Display-only and Database-free modes.
            skin	       : '../skins/skin.png', 		//path to skin file relative to HTML document
            animate        : true,       //apply animation
            textmain       : '%ms (%v votes)',      //main text, shown then stats are loaded
            texthover      : 'very bad|bad|average|good|excellent'
        });
    });